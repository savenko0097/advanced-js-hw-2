'use strict';

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


//add list item
function createList(array){

    let div = document.createElement('div');
    div.setAttribute('id' ,  "root");
    document.body.append(div);

    let ul = document.createElement('ul');
    ul.style.fontSize = "20px";
    ul.style.color = "#454545"
    div.prepend(ul);


    books.forEach(item => {
        const { author, name, price } = item;
        
        if (author && name && price) {
          const li = document.createElement("li");
          li.textContent = `Автор: ${author}; Назва: ${name}, Ціна: ${price}₴`;
          ul.append(li);
        } else{
          throw new Error();
        }
      });
}



// check keys in object
function checkKeys(obj) {
  const keys = ['author', 'name', 'price'];

  for (let key of keys) {
    if (!obj.hasOwnProperty(key)) {
      return key;

    }
  }

  return null;
  
}


try{
createList(books);
} catch{
for (let obj of books) {
  const missingKey = checkKeys(obj);

  if (missingKey !== null) {
    console.log(`Pease, add: ${missingKey}`);
  }
}

}

