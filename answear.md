## Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.



class DivideError extends Error{
    constructor(name){
        super(name);
        this.name = "DivideError"
    }
}

function divide(a, b) {
    try {
      if (a !==0 || b !== 0) {
        return a / b;
      } else throw new DivideError("Cant divide 0 to 0")
    } catch (error) {
      console.log(error);;
    }
  }
  
  console.log(divide(10, 2)); // 5
  console.log(divide(10, 0)); // infinity
  console.log(divide(0, 0)); // DivideError: Cant divide 0 to 0