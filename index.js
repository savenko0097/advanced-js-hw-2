'use strict';

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];




function createList(){

    let div = document.createElement('div');
    div.setAttribute('id' ,  "root");
    document.body.append(div);

    let ul = document.createElement('ul');
    ul.style.fontSize = "20px";
    ul.style.color = "#454545"
    div.prepend(ul);


    books.forEach(item => {
        const { author, name, price } = item;
        
        if (author && name && price) {
          const li = document.createElement("li");
          li.textContent = `Автор: ${author}; Назва: ${name}, Ціна: ${price}₴`;
          ul.append(li);
        } else {
          console.error("Not enough info:", item);
        }
      });
}


try{
createList()
} catch(e){
  console.log(e);
}
